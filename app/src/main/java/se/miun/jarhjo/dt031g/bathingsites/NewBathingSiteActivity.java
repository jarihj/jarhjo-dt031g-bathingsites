package se.miun.jarhjo.dt031g.bathingsites;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;


public class NewBathingSiteActivity extends AppCompatActivity implements WeatherReader, BathingSiteReader {


    private BathingSitesFragment bathingSitesFragment = null;
    MenuItem miClear, miSave, miWeather, miWeatherImportPosition, miSettings, miDeleteAll;
    boolean bImportPosition = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_bathing_site);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_new_bathing_site_menu, menu);
        miClear = menu.findItem(R.id.miClear);
        miClear.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                miClearClick(item);
                return true;
            }
        });
        miSave = menu.findItem(R.id.miSave);
        miSave.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                miSaveClick(item);
                return true;
            }
        });

        miWeather = menu.findItem(R.id.miWeather);
        miWeather.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                miWeatherClick(item);
                return false;
            }
        });
        miWeatherImportPosition = menu.findItem(R.id.miWeatherImportPosition);
        miWeatherImportPosition.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                miWeatherImportPositionClick(item);
                return false;
            }
        });

        miSettings = menu.findItem(R.id.miSettings);
        miSettings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                miSettingsClick(item);
                return false;
            }
        });

        miDeleteAll = menu.findItem(R.id.miDeleteAll);
        miDeleteAll.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                miDeleteAllClick(item);
                return false;
            }
        });


        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        bathingSitesFragment =  (BathingSitesFragment) getSupportFragmentManager().findFragmentById(R.id.bathingSitesFragment_nbs);
        updateBathingSiteCount();
    }

    public void updateBathingSiteCount() {
        BathingSitesDBEngine dbe = new BathingSitesDBEngine(this,this);
        dbe.execute(new BathingSiteEntry(BSDBEOperation.count,null));
    }


    public void miSaveClick(MenuItem item) {
        NewBathingSiteFragment ff =  (NewBathingSiteFragment) getSupportFragmentManager().findFragmentById(R.id.newBathingSiteFragment);
        if (ff.verifyContent())
            saveNewBathingSite(ff);
    }

    public void miClearClick(MenuItem item) {
        clear();
    }

    public void clear() {
        NewBathingSiteFragment ff =  (NewBathingSiteFragment) getSupportFragmentManager().findFragmentById(R.id.newBathingSiteFragment);
        ff.clear();
    }

    public void miWeatherClick(MenuItem item) {
        Log.d("NewBathingSiteActivity","miWeatherClick: ");
        String baseURL = SettingsActivity.getWeatherServerURL(this);
        String url = "";
        String param = "";
        NewBathingSiteFragment ff =  (NewBathingSiteFragment) getSupportFragmentManager().findFragmentById(R.id.newBathingSiteFragment);
        if (ff.getAddress().length()>0) {
            param = "?q=" + ff.getAddress();
//            param=TextUtils.htmlEncode(param); // Does not seem to work - the server responds poorly
//            param=StringUtils.encodeHtml(param); // Does not seem to work - the server responds poorly
        } else if (ff.getLatitude()!=null & ff.getLongitude()!=null) {
            param = String.format("?lat=%f&lon=%f",ff.getLatitude(),ff.getLongitude());
        } else {
            Toast.makeText(this,"Not enough information for weather lookup",Toast.LENGTH_LONG).show();
        }
        if (param.length()>0) {
            url = baseURL + param;
            DownloadWeatherTask dwt = new DownloadWeatherTask(this, this);
            dwt.setContext(this);
            dwt.execute(url);
        }
    }

    public void miWeatherImportPositionClick(MenuItem item) {
        bImportPosition = true;
        miWeatherClick(item);
    }

    public void miDeleteAllClick(MenuItem item) {
        NewBathingSiteFragment ff =  (NewBathingSiteFragment) getSupportFragmentManager().findFragmentById(R.id.newBathingSiteFragment);
        deleteAllBathingSites(ff);
    }

    public boolean miSettingsClick(MenuItem item) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        return false;
    }



    private void saveNewBathingSite(NewBathingSiteFragment ff) {
        String str = String.format("Name: %s\nDescription: %s\nAddress: %s\nLattitude: %f\nLongitude: %f\nGrade: %f\nWater temp.: %f\nDate: %s",
                ff.getName(),ff.getDesc(),ff.getAddress(),ff.getLatitude(),ff.getLongitude(),ff.getGrade(),ff.getTemp(),ff.getDate().toString());
        Log.d("BathingSitesDatabase","save:"+str);
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        Double latitude = ff.getLatitude();
        Double longitude = ff.getLongitude();
        Float grade = ff.getGrade();
        Float temp = ff.getTemp();
        Date date = ff.getDate();
        BathingSite bathingSite = new BathingSite(ff.getName(),ff.getDesc(),ff.getAddress(),latitude,longitude,grade,temp,date);

        BathingSitesDBEngine dbe = new BathingSitesDBEngine(this,this);
        dbe.execute(new BathingSiteEntry(BSDBEOperation.add,bathingSite));
    }


    private void deleteAllBathingSites(NewBathingSiteFragment ff) {
        Log.d("BathingSitesDatabase","deleteAllBathingSites: ");
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.deleteAll,null));
    }


    @Override
    public void onReadWeatherData(WeatherData weatherData)  {
        onReadWeatherData(weatherData,null);
    }


    @Override
    public void onReadWeatherData(WeatherData wd, Drawable drawable) {
        Log.d("NewBathingSiteActivity","readWeatherData: "+wd.name);
        String strTitle = getResources().getString(R.string.weather_current_weather_title);
        String strMessage = getResources().getString(R.string.weather_current_weather_message_format);
        strMessage = String.format(strMessage,wd.name,wd.latitude,wd.longitude,wd.description,wd.temperature);
        Log.d("NewBathingSiteActivity","readWeatherData: strMessage: "+strMessage);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this).setTitle(strTitle).setMessage(strMessage);
        if (drawable!=null) {alertDialogBuilder = alertDialogBuilder.setIcon(drawable);}
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.lblOK), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }
        ).show();
        if (bImportPosition && wd.latitude!=null && wd.latitude!=null ){
            NewBathingSiteFragment ff =  (NewBathingSiteFragment) getSupportFragmentManager().findFragmentById(R.id.newBathingSiteFragment);
            ff.setLatitudeText(""+wd.latitude);
            ff.setLongitudeText(""+wd.longitude);
        }
        bImportPosition=false;
    }

    @Override
    public void onBathingSiteAdd(BathingSite bathingSite) {
        if (bathingSite!=null) {
            Toast.makeText(this,getResources().getString(R.string.new_site_added),Toast.LENGTH_LONG).show();
            Log.d("NewBathingSiteActivity", "bathingSiteAddDone: " + bathingSite.id + " " + bathingSite.name);
        }
        else {
            Log.d("NewBathingSiteActivity", "bathingSiteAddDone: NULL");
        }
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.getAll,null));
        updateBathingSiteCount();
        clear();
        xSleep(250);
        finish();

    }



    @Override
    public void onBathingSiteCount(int bathingSiteCount) {
        if (bathingSitesFragment!=null)
            bathingSitesFragment.updateCountLabel(bathingSiteCount);
        Log.d("NewBathingSiteActivity","bathingSiteCount: "+bathingSiteCount);

    }

    @Override
    public void onBathingSiteDelete() {
        updateBathingSiteCount();
        Toast.makeText(this,getResources().getString(R.string.all_sites_deleted),Toast.LENGTH_LONG).show();
        Log.d("NewBathingSiteActivity","bathingSiteDeleteDone: ");

    }

    @Override
    public void onBathingSiteRead(List<BathingSite> bathingSites) {
        Cache.setBathingSites(bathingSites);
        Log.d("NewBathingSiteActivity","bathingSiteRead: "+bathingSites.size());
    }

    private void xSleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
