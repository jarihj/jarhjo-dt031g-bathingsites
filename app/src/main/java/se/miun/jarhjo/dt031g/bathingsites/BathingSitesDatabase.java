package se.miun.jarhjo.dt031g.bathingsites;

import android.content.Context;
import android.util.Log;

import java.util.Date;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.*;


public class BathingSitesDatabase {
    private static BathingSitesDatabase bathingSitesDatabase = null;
    private Context context = null;
    private AppDatabase db;

    private BathingSitesDatabase(Context context) {
        this.context = context;
        db = Room.databaseBuilder(context,AppDatabase.class, "BathingSitesDatabase").build();
    }

    public static BathingSitesDatabase getInstance(Context context) {
        if (bathingSitesDatabase==null) {
            bathingSitesDatabase = new BathingSitesDatabase(context);
        }
        return bathingSitesDatabase;
    }

    public static void destroyInstance() {
        bathingSitesDatabase = null;
    }


    public void addBathingSite(BathingSite bs) {
//        if (bs.temp==null)
//            Log.d("BathingSitesDatabase","addBathingSite bs temp IS null");
//        Log.d("BathingSitesDatabase",String.format("Data entry: (%d,%s,%s,%s,%f,%f,%f,%f,%d)",bs.id,bs.name,bs.desc,bs.address,bs.latitude,bs.longitude,bs.grade,bs.temp,bs.date));
        db.bathingSiteDao().addBathingSite(bs);
        Log.d("BathingSitesDatabase","addBathingSite done");
    }

    public int countBathingSites() {
        return db.bathingSiteDao().count();
    }

    public List<BathingSite> getAll() {
//        Log.d("BathingSitesDatabase",String.format("begin"));
        List<BathingSite> bathingSites =  db.bathingSiteDao().getAll();
        if (bathingSites!=null) {
            int n = bathingSites.size();
            Log.d("BathingSitesDatabase", String.format("Number of records: %d", n));
//            for (int i=0; i<bathingSites.size();i++) {
//                Log.d("BathingSitesDatabase", String.format("Data %d : (%s,%s,%f,%f)",i,bathingSites.get(i).name,bathingSites.get(i).date,bathingSites.get(i).latitude,bathingSites.get(i).longitude));
//            }
        }
        else {
            Log.d("BathingSitesDatabase", String.format("Number of records: NULL"));
        }
        Log.d("BathingSitesDatabase",String.format("end"));
        return bathingSites;
    }

    public void deleteAll() {
        db.bathingSiteDao().deleteAll();
    }
}

//https://developer.android.com/reference/android/arch/persistence/room/Entity
@Entity (primaryKeys = {"latitude","longitude"})
class BathingSite {
//    @PrimaryKey
    @Nullable public Long id;
    @NonNull public String name;
    @Nullable public String desc = null;
    @Nullable public String address = null;
    @NonNull public Double latitude;
    @NonNull public Double longitude;
    @Nullable public Float grade = null;
    @Nullable public Float temp = null;
    // https://www.tutorialspoint.com/java/java_date_time.htm
    @Nullable public Long date = null; //  The number of milliseconds that have elapsed since January 1, 1970.

    public BathingSite() {
    }

    public BathingSite(String name, String desc, String address, Double latitude, Double longitude, Float grade, Float temp, Date date) {
//        this(BathingSitesDBEngine.getIDmillis(),name,desc,address,latitude,longitude,grade,temp,date);
        this(null,name,desc,address,latitude,longitude,grade,temp,date);
    }


    public BathingSite(Long id,String name, String desc, String address, Double latitude, Double longitude, Float grade, Float temp, Date date) {
        this.id = id;
        if (this.id==null) {
            this.id = BathingSitesDBEngine.getIDmillis();
            if (latitude!=null)
                this.id+=(long) (latitude*(-1)*1e12);
            if (longitude!=null)
                this.id+=(long) (longitude*(+1)*1e6);
            this.id+=(new Random()).nextLong();
        }
        this.name =name;
        this.desc = desc;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.grade = grade;
        this.temp = temp;
        this.date = null;
        this.date = date.getTime();
    }

    @Override
    public String toString() {
        return String.format("(%s,%f,%f)",name,latitude,longitude);
    }
}


@Dao
interface BathingSiteDao {
    @Query("SELECT * FROM bathingSite")
    List<BathingSite> getAll();

    @Query("SELECT * FROM BathingSite WHERE id IN (:bathingSiteIds)")
    List<BathingSite> loadAllByIds(int[] bathingSiteIds);

    @Query("SELECT count(*) as count FROM bathingSite")
    int count();

    @Insert
    void insertAll(BathingSite... bathingSites);

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addBathingSite(BathingSite bathingSite);
//
//    @Delete
//    void delete(BathingSite bathingSite);

    @Query("delete from bathingSite WHERE id = :bathingSiteId")
    void deleteId(long bathingSiteId);

    @Query("delete from bathingSite")
    void deleteAll();

}

// https://developer.android.com/training/data-storage/room/index.html
@Database(entities = {BathingSite.class}, version = 1)
abstract class AppDatabase extends RoomDatabase {
    public abstract BathingSiteDao bathingSiteDao();
}
