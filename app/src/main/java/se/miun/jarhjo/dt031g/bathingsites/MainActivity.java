package se.miun.jarhjo.dt031g.bathingsites;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements BathingSiteReader, WeatherReader {
    public final static int FINE_LOCATION_PERMISSION_REQUEST_CODE = 203;
    public final static int COARSE_LOCATION_PERMISSION_REQUEST_CODE = 204;
    private LocationListener locationListener;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;


    private Handler handler = null;  // For timed update
    private Runnable runnable = null; // for timed update
    private Double latitude = null, longitude = null;
    private MenuItem miSettings;
    private TextView tvGPSPosition;
    private FloatingActionButton fab;
    private BathingSitesFragment bathingSitesFragment;
    private char dots = '-';
    private final int interval = 5000;
    private LatLng myPos = null;
    private boolean timedUpdates = false;
    private ImageView ivBathingSitesImage;
    private int bathingSiteCount = -1;
    private BathingSite currentSite = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fab = findViewById(R.id.fab);
        bathingSitesFragment = (BathingSitesFragment) getSupportFragmentManager().findFragmentById(R.id.bathingSitesFragment);
//        requestLocationPermission();
        updateBathingSiteCount();
        loadBathingSites();
        ivBathingSitesImage = findViewById(R.id.ivBathingSitesImage);
        ivBathingSitesImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBathingSitesImageClick(v);
            }
        });

        //================
        fusedLocationClient  = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("MapActivity", "LocationListener.onLocationChanged: "+location.getLatitude()+"  "+location.getLongitude());
                setLatLon(location.getLatitude(),location.getLongitude());
                updateGPSLabel();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) { }

            @Override
            public void onProviderEnabled(String provider) { }

            @Override
            public void onProviderDisabled(String provider) { }
        };
        permissionCheckLocation();

    }

    private void ivBathingSitesImageClick(View v) {
        int index = new Random().nextInt(Cache.getCount());
        if (index<=Cache.getBathingSites().size()) {
            currentSite = Cache.getBathingSites().get(index);
             if (currentSite.latitude!=null & currentSite.longitude!=null) {
                 String url=SettingsActivity.getWeatherServerURL(this)+String.format("?lat=%f&lon=%f",currentSite.latitude,currentSite.longitude);
                 new DownloadWeatherTask(this,this).execute(url);
            } else {
                Toast.makeText(this,"Not enough information for weather lookup",Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this,"Index too large. Update or restart the app.",Toast.LENGTH_LONG).show();
        }

    }

    private void loadBathingSites() {
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.getAll,null));
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (timedUpdates) {
            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    /* do what you need to do */
                    tvGPSPosition = findViewById(R.id.tvGPSPosition);
                    permissionCheckLocation();
                    updateGPSLabel();
                    /* and here comes the "trick" */
                    int interval = getResources().getInteger(R.integer.gps_timer);
                    handler.postDelayed(this, interval); //
                }
            };
            handler.postDelayed(runnable, interval);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBathingSiteCount();
        loadBathingSites();
    }

    private void updateGPSLabel() {
        if (tvGPSPosition!=null) {
            if (getLatitude() != null & getLongitude() != null) {
                iterateDots();
                String str = getResources().getString(R.string.gps_mainactivity_position_format) + " " + dots;
                tvGPSPosition.setText(String.format(str, getLatitude(), getLongitude()));
            } else {
                iterateDots();
                tvGPSPosition.setText(dots);
            }
        }

    }

    private void iterateDots() {
        switch (dots) {
            case '-': dots='\\'; break;
            case '\\': dots='|'; break;
            case '|':  dots='/'; break;
            case '/':  dots='-'; break;
        }
    }

    public void updateBathingSiteCount() {
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.count,null));
    }

    public void fabClick(View view) {
        Intent intent = new Intent(this,se.miun.jarhjo.dt031g.bathingsites.NewBathingSiteActivity.class);
        startActivity(intent);
        Snackbar.make(view, getResources().getString(R.string.add_new_bathing_site_toast), Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        miSettings = menu.findItem(R.id.miSettings);

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case FINE_LOCATION_PERMISSION_REQUEST_CODE:
                Log.d("MainActivity.OnRequestP", String.format("Fine Location Permissions: [grantResults:%s] [permissions[]:%s]",StringUtils.intArrToString(grantResults),StringUtils.strArrToString(permissions)));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 50, locationListener);
//                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,50, locationListener);
                break;
            case COARSE_LOCATION_PERMISSION_REQUEST_CODE:
                Log.d("MainActivity.OnRequestP", String.format("Coarse Location Permissions: [grantResults:%s] [permissions[]:%s]",StringUtils.intArrToString(grantResults),StringUtils.strArrToString(permissions)));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 50, locationListener);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,50, locationListener);
                break;
            default:
                Log.d("DialActivity.OnRequestP", String.format("onRequestPermissionsResult: IMPOSSIBLE EVENT: [grantResults:%s] [permissions[]:%s]",StringUtils.intArrToString(grantResults),StringUtils.strArrToString(permissions)));
        }
    }

    /**
     * This method checks the location permission. If location permission isn't given, then ask for it...
     */
    public void permissionCheckLocation() {
        int permissionCheckLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if(permissionCheckLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.FINE_LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            // Requesting location update based on minimum distance in meters
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,50, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,50, locationListener);
        }
    }

    public boolean checkLocationPermission() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public void requestLocationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_PERMISSION_REQUEST_CODE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, COARSE_LOCATION_PERMISSION_REQUEST_CODE);
    }

    public boolean miMapClick(MenuItem item) {
        Intent intent = new Intent(this, se.miun.jarhjo.dt031g.bathingsites.MapActivity.class);
        intent.putExtra("lat",getLatitude());
        intent.putExtra("lon",getLongitude());
        startActivity(intent);
        return false;
    }


    public boolean miSettingsClick(MenuItem item) {
        Intent intent = new Intent(this, se.miun.jarhjo.dt031g.bathingsites.SettingsActivity.class);
        startActivity(intent);
        return false;
    }

    public boolean miDownloadBathingSitesClick(MenuItem item) {
        Intent intent = new Intent(this, se.miun.jarhjo.dt031g.bathingsites.DownloadActivity.class);
        intent.putExtra("url",SettingsActivity.getBathingSiteDownloadURL(getApplication()));
        startActivity(intent);
        return false;
    }

    public boolean miImportFaroeseSitesClick(MenuItem item) {
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Koltur",null,null,62.0d,-7.0,3.0f,5.29f,Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Tórshavn",null,null,62.0009,-6.7772,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Toftir",null,null,62.0963,-6.7410,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Toftavatn","Lítlavatn, brúgvin","Toftavatn",62.0999,-6.7212,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Toftavatn","Stóravatn, Borgin","Toftavatn",62.0948,-6.7171,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Toftavatn","Stóravatn, Sandurin Mikli","Toftavatn",62.0920,-6.7053,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Klaksvík",null,null,62.2329,-6.5866,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Saksun",null,null,62.2477,-7.1849,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Mykines",null,null,62.0963,-7.6348,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Tvøroyri",null,null,61.5563,-6.8157,3.0f,5.29f, Calendar.getInstance().getTime())));
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.add,new BathingSite("Trongisvágur",null,null,61.5606,-6.8410,3.0f,5.29f, Calendar.getInstance().getTime())));
        return false;
    }

    @Override
    public void onBathingSiteAdd(BathingSite bathingSite) {
        updateBathingSiteCount();
        loadBathingSites();
    }

    @Override
    public void onBathingSiteCount(int bathingSiteCount) {
        Cache.setCount(bathingSiteCount);
        if (bathingSitesFragment!=null)
            bathingSitesFragment.updateCountLabel(Cache.getCount());
    }

    @Override
    public void onBathingSiteDelete() {

    }

    @Override
    public void onBathingSiteRead(List<BathingSite> bathingSites) {
        Cache.setBathingSites(bathingSites);
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    private void updateMyPos() {
        if (this.latitude!=null && this.longitude!=null)
            myPos = new LatLng(latitude,longitude);
    }

    public void setLatLon(double lat, double lon) {
        this.latitude=lat;
        this.longitude=lon;
        updateMyPos();
    }


    @Override
    public void onReadWeatherData(WeatherData weatherData) {
        onReadWeatherData(weatherData,null);
    }

    @Override
    public void onReadWeatherData(WeatherData wd, Drawable drawable) {
        if (currentSite!=null) {
            Log.d("NewBathingSiteActivity","readWeatherData: "+wd.name);
            String strTitle = getResources().getString(R.string.random_site_title);
            strTitle=currentSite.name;
            String strMessage = "";
//            strMessage += String.format(getResources().getString(R.string.random_site_name_format),currentSite.name);
            if (currentSite.desc!=null && currentSite.desc.length()>0)
                strMessage += String.format(getResources().getString(R.string.random_site_description_format),currentSite.desc);
            if (currentSite.address!=null && currentSite.address.length()>0)
                strMessage += String.format(getResources().getString(R.string.random_site_address_format),currentSite.address);
            if (currentSite.latitude!=null && currentSite.longitude!=null)
                strMessage += String.format(getResources().getString(R.string.random_site_latlon_format),currentSite.latitude,currentSite.longitude);
            if (wd!=null)
                strMessage += String.format(getResources().getString(R.string.random_site_current_weather));
            if (wd!=null && wd.description!=null && wd.description.length()>0)
                strMessage += String.format(getResources().getString(R.string.random_site_weather_description_format),wd.description);
            if (wd!=null && wd.temperature!=null)
                strMessage += String.format(getResources().getString(R.string.random_site_weather_temperature_format),wd.temperature);

            Log.d("NewBathingSiteActivity","readWeatherData: strMessage: "+strMessage);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this).setTitle(strTitle).setMessage(strMessage);
            if (drawable!=null) {alertDialogBuilder = alertDialogBuilder.setIcon(drawable);}
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.lblOK), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }
            ).show();
            currentSite=null;
        }
    }
}
