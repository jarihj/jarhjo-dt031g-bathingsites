package se.miun.jarhjo.dt031g.bathingsites;

import java.util.List;

public class Cache {
    private static List<BathingSite> bathingSites = null;
    private static int count = -1;

    public static List<BathingSite> getBathingSites() {
        return bathingSites;
    }

    public static void setBathingSites(List<BathingSite> value) {
        bathingSites = value;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int value) {
        count=value;
    }
}
