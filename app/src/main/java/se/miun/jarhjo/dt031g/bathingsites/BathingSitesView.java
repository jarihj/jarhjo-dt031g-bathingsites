package se.miun.jarhjo.dt031g.bathingsites;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;


public class BathingSitesView extends ConstraintLayout {
    private int bathingSitesNumber = 0;
    private TextView tvText;

    public BathingSitesView(Context context) {
        super(context);
        init(context, null);
    }

    public BathingSitesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BathingSitesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Initilizes the view
     * @param context - The context of the view
     * @param attrs - an attribute set
     */
    @SuppressLint("ClickableViewAccessibility")
    private void init(Context context, AttributeSet attrs) {
        // SET INFLATED LAYOUT
        inflate(context, R.layout.bathing_sites_view, this);
        tvText = findViewById(R.id.tvBathingSitesNumber);

        setBathingSitesCount(-1);
    }

    public int getBathingSitesNumber() {
        return bathingSitesNumber;
    }

    public void setBathingSitesCount(int bathingSitesNumber) {
        this.bathingSitesNumber = bathingSitesNumber;
        if (tvText!=null)
            tvText.setText(String.format(getResources().getString(R.string.number_of_bathing_sites_format),bathingSitesNumber));

    }
}
