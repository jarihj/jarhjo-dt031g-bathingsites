package se.miun.jarhjo.dt031g.bathingsites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;
import java.util.stream.Stream;


public class DownloadActivity extends AppCompatActivity {
    public static final int CALL_PERMISSION_REQUEST_CODE = 303;
    private class DownloadWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Toast.makeText(getApplicationContext(),"URL: "+url,Toast.LENGTH_LONG).show();
            Log.d("DownloadWVC.shouldOverr","URL: "+url);
            return false;
//            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            startActivity(intent);
//            return true;
        }
    }

    private class DownloadTask extends AsyncTask<String, Integer, Long> {
        Context context = null;
//        String saveFile; // full path
//        String fileName; // only filename

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("DownloadTask.onPre","starting");
            progressBar.setMax(100);
            progressBar.setProgress(0);
            getTvProgressPercent.setText("0 %");
            showProgressView();
        }

//        @SuppressLint("WrongThread")
        protected Long doInBackground(String... urls) {
            String url = urls[0];
            long writtenBytes = 0;
            int bytesRead = -1;
            int bytesSize = 0;

            try {
                Log.d("DownloadTask.doInBack","url: "+url);
                URL u = new URL(url);
                URLConnection urlConnection = null;
                urlConnection = u.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
//                fileName = url.substring(url.lastIndexOf('/')+1, url.length());
//                saveFile = "tmp.csv";
//                Log.d("DownloadTask.doInBack","saveFile: "+saveFile);
                StringBuilder sb = new StringBuilder();
//                FileOutputStream outputStream = new FileOutputStream(saveFile);
                char[] buffer = new char[1024];
                bytesSize = urlConnection.getContentLength();
                Log.d("DownloadTask.doInBack","before setprogress");

                Integer [] pub = {0,1};
                String str = "";
                int iMethod = 1;
                switch (iMethod) {
                    case 1 : {
                        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                        String sl;
                        int i = 1;
                        while ((sl = br.readLine()) != null) {
                            str+=sl+"\n";
                            bytesRead=sl.length();
                            writtenBytes+=bytesRead;
                            pub[0] = (int) writtenBytes;
                            pub[1] = bytesSize;
                            publishProgress(pub);
                            Thread.sleep(0);
                            Log.d("DownloadTask","whileDownload: "+i+":"+sl);
                            BathingSite bs = BathingSiteParser.parse(sl);
                            i++;
//                            if (i>3) break;
                        }
                        inputStream.close();
                        break;
                    }
                    case 2 :  {
                        // Method 2
                        int i = 1;
                        InputStreamReader inputStreamReader =  new InputStreamReader(inputStream);
                        while ((bytesRead=inputStreamReader.read(buffer))!=-1) {
                            sb.append(buffer);
                            writtenBytes+=bytesRead;
                            pub[0] = (int) writtenBytes;
                            pub[1] = bytesSize;
                            publishProgress(pub);
                            Thread.sleep(0);
                            i++;
                        }
                        inputStreamReader.close();
                        str=sb.toString();
                        break;
                    }

                }


                // Now parse the read String
                InputStream is = new ByteArrayInputStream(str.getBytes());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
                String s = "";
                int j = 1, iSuccess = 0;
                writtenBytes = 0;
                while ((s = bufferedReader.readLine())!=null) {
                    //Log.d("DownloadTask",j+":"+s);
                    try {
                        BathingSite bs = BathingSiteParser.parse(s);
                        new BathingSitesDBEngine(context,null).execute(new BathingSiteEntry(BSDBEOperation.add,bs));
                        iSuccess++;
                        writtenBytes+=(s.length()+1);
                        pub[0] = (int) writtenBytes;
                        pub[1] = bytesSize;
                        publishProgress(pub);
                    } catch (Exception e) {
                        Log.d("DownloadTask","error on "+j);
                        Log.d("DownloadTask","error on "+s);
                    }
                    j++;
//                    if (j>3) break;
                }
                bufferedReader.close();is.close();

                Log.d("DownloadTask.doInBack",String.format("Download finished. %d bytes written. %d sites added",writtenBytes,iSuccess));



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return writtenBytes;
        }

        protected void onProgressUpdate(Integer... progress) {
            setProgressMax(progress[1]);
            setProgressPosition(progress[0]);
        }

        protected void onPostExecute(Long result) {
            Log.d("DownloadTask.onPost",String.format("Download finished. %d bytes written.",result));
            hideProgressView();
        }

    }
    private Context context = null;
    private String storageLocation;
    private String url;
    private WebView wvWebView;
    private DownloadWebViewClient webViewClient;
    private ConstraintLayout clDownload;
    private ProgressBar progressBar;
    private TextView tvProgressText;
    private TextView getTvProgressPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_download);
        wvWebView = findViewById(R.id.wvWebView);
        webViewClient = new DownloadWebViewClient();
        wvWebView.setWebViewClient(webViewClient);
        wvWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Toast.makeText(getApplicationContext(),"URL: "+url,Toast.LENGTH_LONG).show();
                Log.d("DownloadActivity.onDown","URL: "+url);
                tvProgressText.setText(String.format("Downloading %s ...",url.substring(url.lastIndexOf('/')+1, url.length())));
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.context = context;
                downloadTask.execute(url);
            }
        });
        clDownload = (ConstraintLayout) findViewById(R.id.clDownload);
        tvProgressText = (TextView) findViewById(R.id.tvProgressText);
        getTvProgressPercent = (TextView) findViewById(R.id.tvProgressPercent);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUrl(getIntent().getExtras().getString("url"));
        wvWebView.loadUrl(getUrl());
        Log.d("DialActivity.onCreate","Write external storage granted.");

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CALL_PERMISSION_REQUEST_CODE :
                Log.d("DownloadAct.OnRequestP","Call Permissions: [["+grantResults+"]] "+permissions);
                break;
            default:
                Log.d("DownloadAct.OnRequestP","SHOULD NOT OCCUR!!!! Permissions: [["+grantResults+"]] "+permissions);
        }
    }

    private void showProgressView() {
        clDownload.setVisibility(View.VISIBLE);
        clDownload.bringToFront();
    }

    private void hideProgressView() {
        clDownload.setVisibility(View.INVISIBLE);
    }

    public void btnClick(View view) {

        if (clDownload.getVisibility()==View.VISIBLE)  {
            hideProgressView();
        } else {
            showProgressView();
        }
    }

    private void setProgressPosition(int value) {
        progressBar.setProgress((int)  value);
        getTvProgressPercent.setText(String.format("%.0f %%",100.0*progressBar.getProgress()/progressBar.getMax()));
    }

    private void setProgressMax(int value) {
        progressBar.setMax((int) value);
    }

    private void setProgressText(String value) {
        tvProgressText.setText(value);
    }


}



