package se.miun.jarhjo.dt031g.bathingsites;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

class InvalidChoiceException extends Exception {
    public InvalidChoiceException(String s) {
        super(s);
    }
};

//public class MapActivity extends DialogFragment implements OnMapReadyCallback, BathingSiteReader{
public class MapActivity extends FragmentActivity implements OnMapReadyCallback, BathingSiteReader {
    public static Context mapContext = null;

    private GoogleMap mMap;
    private Double lat, lon = null;
    private LatLng myPos = null;
//    private List<BathingSite> bathingSites = null;
    private SupportMapFragment mapFragment;
    private LocationListener locationListener;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;
    Polyline polyline = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mapContext = this;
        super.onCreate(savedInstanceState);
        setLat(getIntent().getExtras().getDouble("lat"));
        setLon(getIntent().getExtras().getDouble("lon"));
        myPos = new LatLng(getLat(), getLon());
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
//================
        fusedLocationClient  = LocationServices.getFusedLocationProviderClient(this);

        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("MapActivity", "LocationListener.onLocationChanged: "+location.getLatitude()+"  "+location.getLongitude());
                mMap.clear();
                setLatLon(location.getLatitude(),location.getLongitude());
                drawMap(mMap);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) { }

            @Override
            public void onProviderEnabled(String provider) { }

            @Override
            public void onProviderDisabled(String provider) { }
        };
        permissionCheckLocation();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        drawMap(googleMap);
    }

    private void drawMap(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.addMarker(new MarkerOptions().position(myPos).title(getResources().getString(R.string.you_are_here)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(myPos,getZoomFromRadius(SettingsActivity.getMapRadius(this))));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPos,getZoomFromRadius(SettingsActivity.getMapRadius(this))));
        // Drawing circle on the map
        drawCircle(myPos);
        loadBathingSites();
    }

    private float getZoomFromRadius(double radius) {
        double theFactor = 6.3;
        double km = radius*theFactor;
        float zoom = (float) log2(40000.0d / ( km / 2.0d));
        Log.d("MapActivity", "km: "+km +" zoom: "+zoom);
        return zoom;
    }

    private double log2(double x) {
        return Math.log(x)/Math.log(2);
    }

    private void loadBathingSites() {
        new BathingSitesDBEngine(this,this).execute(new BathingSiteEntry(BSDBEOperation.getAll,null));
    }

    private void drawBathingSites() {
        LatLng pos=null;
        BathingSite bs = null;
        MarkerOptions mo = null;
        Marker ma = null;
        if (Cache.getBathingSites()!=null) {
            for (int i=0;i<Cache.getBathingSites().size();i++) {
                bs=Cache.getBathingSites().get(i);
                if (bs.latitude>200) {
                    bs.latitude=null;
                    bs.longitude=null;
                    Log.d("MapActivity","Filtered null-lat:"+i+" "+bs.name);
                }
                if (bs.latitude!=null && bs.longitude!=null) {
                    if (distanceTo(bs.latitude,bs.longitude) < (SettingsActivity.getMapRadius(this)*1000)) {
//                        Log.d("MapActivity", "Marker:" + i + " ["+bs.id+"] " + bs.toString());
                        double x = bs.latitude;
                        double y = bs.longitude;
                        pos = new LatLng(x, y);
                        mo = new MarkerOptions().position(pos)
//                                .snippet(getResources().getString(R.string.click_for_info))
                                .snippet(String.format("%.1f km",(distanceTo(bs.latitude,bs.longitude)/1000)))
                                .title(bs.name);
                        ma=mMap.addMarker(mo);
                        ma.setTag((long) i);
                    }
                }
//                if (i>50) break;
            }
        } // if

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                int iFound =-1;
                if (marker.getTag()!=null) {
                    iFound = (int)((long) marker.getTag());
                }
                if (iFound>-1) {
                    BathingSite bs = Cache.getBathingSites().get(iFound);
                    Log.d("MapActivity", "onInfoWindowClick:" + iFound + " iFound");
                    if (polyline != null) {
                        polyline.remove();
                        polyline = null;
                    }
                    // OLD METHOD
//                    PolylineOptions polylineOptions = new PolylineOptions();
//                    polylineOptions.add(new LatLng(bs.latitude, bs.longitude))
//                            .add(myPos);


                    // NEW METHOD
                    LatLng p = new LatLng(bs.latitude,bs.longitude);
                    PolylineOptions polylineOptions = new PolylineOptions();
//                    polylineOptions.color(0xFF0000);
                    polylineOptions.width(5.0F);
                    polylineOptions.add(myPos);
                    for (int i=1;i<=25;i++) {
                        polylineOptions.add(DistanceCalculator.intermediatePoint(myPos,p,(double) i/25.0d));
                    }
                    polyline = mMap.addPolyline(polylineOptions);
                }
                marker.showInfoWindow();
                return true;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Log.d("MapActivity","onInfoWindowClick:"+marker.getTitle()+"["+((long) marker.getTag())+"]");
                int iFound = -1;
                if (marker.getTag()!=null) {
                    iFound = (int)((long) marker.getTag());
                }
                if (iFound>-1) {
                    BathingSite bs = Cache.getBathingSites().get(iFound);
                    Log.d("MapActivity","onInfoWindowClick:"+iFound+" iFound");

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mapContext).setTitle(Cache.getBathingSites().get(iFound).name).setMessage(getMapSummary(Cache.getBathingSites().get(iFound)));
                    alertDialogBuilder.setPositiveButton(getResources().getString(R.string.lblOK), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }
                    ).show();
                }
            }
        });
    }

    /**
     *
     * @param latitude
     * @param longitude
     * @return distance to lotitude,Longitude from current position in meters
     */
    private double distanceTo(Double latitude, Double longitude) {
        return distanceBetween(myPos.latitude,myPos.longitude,latitude,longitude);
    }

    private double distanceBetween(double lat1, double lon1, double lat2, double lon2){
        try {
            return distanceBetween(lat1, lon1, lat2, lon2, 1);
        } catch (InvalidChoiceException e) {
            e.printStackTrace();
            return -1.0;
        }

    }

    private double distanceBetween(double lat1, double lon1, double lat2, double lon2,int algorithm) throws InvalidChoiceException {
        switch (algorithm) {
            case 0 : {
                float[] res = new float[1];
                Location.distanceBetween(lat1, lon1, lat2, lon2, res);
                return res[0];
            }
            case 1 : {
                return DistanceCalculator.distanceSpheriod(lat1,lon1,lat2,lon2);
            }
            case 2 : {
                return DistanceCalculator.distanceElipsoid(lat1,lon1,lat2,lon2);
            }
            default :
                throw new InvalidChoiceException("Invalid choice of algorithm [0-2]. Found: "+algorithm+".");
        }
    }



    private void drawCircle(LatLng point){

        // Instantiating CircleOptions to draw a circle around the marker
        CircleOptions circleOptions = new CircleOptions();

        // Specifying the center of the circle
        circleOptions.center(point);

        // Radius of the circle
        circleOptions.radius(SettingsActivity.getMapRadius(this)*1000);

        // Border color of the circle
        circleOptions.strokeColor(Color.BLACK);

        // Fill color of the circle
//        circleOptions.fillColor(0x30ff0000);

        // Border width of the circle
        circleOptions.strokeWidth(2);

        // Adding the circle to the GoogleMap
        mMap.addCircle(circleOptions);

    }

    public double getLat() {
        return lat;
    }

    private void updateMyPos() {
        if (this.lat!=null && this.lon!=null)
            myPos = new LatLng(lat,lon);
    }

    public void setLatLon(double lat, double lon) {
        this.lat=lat;
        this.lon=lon;
        updateMyPos();
    }

    public void setLat(double lat) {
        this.lat = lat;
        updateMyPos();
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
        updateMyPos();
    }

    @Override
    public void onBathingSiteAdd(BathingSite bathingSite) {

    }

    @Override
    public void onBathingSiteCount(int bathingSiteCount) {

    }

    @Override
    public void onBathingSiteDelete() {

    }

    @Override
    public void onBathingSiteRead(List<BathingSite> bathingSites) {
        Cache.setBathingSites(bathingSites);
        drawBathingSites();
    }

    private String getMapSummary(BathingSite bs) {
        if (bs!=null) {
            StringBuilder sb = new StringBuilder();
//            sb.append(bs.name + "\n");
            if (bs.desc!=null && bs.desc.length() > 0) sb.append(bs.desc + "\n");
            if (bs.address!=null && bs.address.length() > 0) sb.append(bs.address + "\n");
            if (bs.grade != null)
                sb.append(String.format(getResources().getString(R.string.grade_format), bs.grade, 5.0) + "\n");
            return sb.toString().trim();
        } else return "NULL";
    }

    /**
     * This method checks the location permission. If location permission isn't given, then ask for it...
     */
    public void permissionCheckLocation() {
        int permissionCheckLocation;
        permissionCheckLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheckLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.FINE_LOCATION_PERMISSION_REQUEST_CODE);
            Log.d("MainActivity","permissionCheckLocation FINE REQUEST");
        } else {
            // Requesting location update based on minimum distance in meters
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,50, locationListener);
            Log.d("MainActivity","permissionCheckLocation FINE OK");
        }

        // if fine location not granted, try Coarse...
        if (permissionCheckLocation!=PackageManager.PERMISSION_GRANTED)
        permissionCheckLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if(permissionCheckLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MainActivity.COARSE_LOCATION_PERMISSION_REQUEST_CODE);
            Log.d("MainActivity","permissionCheckLocation COARSE REQUEST");
        } else {
            // Requesting location update based on minimum distance in meters
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,50, locationListener);
            Log.d("MainActivity","permissionCheckLocation COARSE OK");
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MainActivity.FINE_LOCATION_PERMISSION_REQUEST_CODE:
                Log.d("MainActivity.OnRequestP", String.format("Fine Location Permissions: [grantResults:%s] [permissions[]:%s]",StringUtils.intArrToString(grantResults),StringUtils.strArrToString(permissions)));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 50, locationListener);
//                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,50, locationListener);
                break;
            case MainActivity.COARSE_LOCATION_PERMISSION_REQUEST_CODE:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Log.d("MainActivity.OnRequestP", String.format("Coarse Location Permissions: [grantResults:%s] [permissions[]:%s]",StringUtils.intArrToString(grantResults),StringUtils.strArrToString(permissions)));
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,50, locationListener);
                break;
            default:
                Log.d("DialActivity.OnRequestP", String.format("onRequestPermissionsResult: IMPOSSIBLE EVENT: [grantResults:%s] [permissions[]:%s]",StringUtils.intArrToString(grantResults),StringUtils.strArrToString(permissions)));
        }
    }

}
