package se.miun.jarhjo.dt031g.bathingsites;

import java.util.List;

public interface BathingSiteReader {

    public void onBathingSiteAdd(BathingSite bathingSite);
    public void onBathingSiteCount(int bathingSiteCount);
    public void onBathingSiteDelete();
    public void onBathingSiteRead(List<BathingSite> bathingSites);
}
