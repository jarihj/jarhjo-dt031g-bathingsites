package se.miun.jarhjo.dt031g.bathingsites;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
class NumberRangeException extends Exception{};

public class NewBathingSiteFragment extends Fragment {
    private final static int iGradedefault = 1;
    private ImageView ivStar1, ivStar2, ivStar3, ivStar4, ivStar5;
    private int grade = 1;
    private EditText etName, etDesc, etAddress, etLatitude, etLongitude, etTemp, etDate;

    public NewBathingSiteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_bathing_site, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etName = view.findViewById(R.id.etName);
        etDesc = view.findViewById(R.id.etDesc);
        etAddress = view.findViewById(R.id.etAddress);
        etLatitude = view.findViewById(R.id.etLatitude);
        etLongitude = view.findViewById(R.id.etLong);
        etTemp = view.findViewById(R.id.etTemp);
        etDate = view.findViewById(R.id.etDate);

        ivStar1 = view.findViewById(R.id.ivStar1);
        ivStar2 = view.findViewById(R.id.ivStar2);
        ivStar3 = view.findViewById(R.id.ivStar3);
        ivStar4 = view.findViewById(R.id.ivStar4);
        ivStar5 = view.findViewById(R.id.ivStar5);

        View.OnClickListener onStarClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivGradeImageClick(v);
            }
        };

        ivStar1.setOnClickListener(onStarClickListener);
        ivStar2.setOnClickListener(onStarClickListener);
        ivStar3.setOnClickListener(onStarClickListener);
        ivStar4.setOnClickListener(onStarClickListener);
        ivStar5.setOnClickListener(onStarClickListener);

        clear();
    }

    public void ivGradeImageClick(View view) {
        Log.d("NewBathingSiteFragment","ivGradeImageClick: begin");

        switch (view.getId()) {
            case (R.id.ivStar1) :
                Log.d("NewBathingSiteFragment","ivGradeImageClick: Star1");
                setGrade(1);
                break;
            case (R.id.ivStar2) :
                Log.d("NewBathingSiteFragment","ivGradeImageClick: Star2");
                setGrade(2);
                break;
            case (R.id.ivStar3) :
                Log.d("NewBathingSiteFragment","ivGradeImageClick: Star3");
                setGrade(3);
                break;
            case (R.id.ivStar4) :
                Log.d("NewBathingSiteFragment","ivGradeImageClick: Star4");
                setGrade(4);
                break;
            case (R.id.ivStar5) :
                Log.d("NewBathingSiteFragment","ivGradeImageClick: Star5");
                setGrade(5);
                break;
            default:
                Log.d("NewBathingSiteFragment","ivGradeImageClick: Default");
                setGrade(0);
        }
        Log.d("NewBathingSiteFragment","ivGradeImageClick: end");
    }

    public boolean verifyContent() {
        boolean result = true;
        if (etName.getText().toString().trim().length()==0) {
            etName.setError(getResources().getString(R.string.err_name_is_empty));
            result=false;
        }
        if ((etAddress.getText().toString().trim().length()==0)&(((etLatitude.getText().toString().trim().length()==0))|((etLongitude.getText().toString().trim().length()==0))))  {
            etAddress.setError(getResources().getString(R.string.err_address_is_empty));
            etLatitude.setError(getResources().getString(R.string.err_latt_is_empty));
            etLongitude.setError(getResources().getString(R.string.err_long_is_empty));
            result=false;
        }
//        if (etLatitude.getText().toString().trim().length()==0) {
//            etLatitude.setError(getResources().getString(R.string.err_latt_is_empty));
//            result=false;
//        }
//        if (etLongitude.getText().toString().trim().length()==0) {
//            etLongitude.setError(getResources().getString(R.string.err_long_is_empty));
//            result=false;
//        }
        return result;
    }

    public void clear() {
        etName.setText("");
        etDesc.setText("");
        etAddress.setText("");
        etLatitude.setText("");
        etLongitude.setText("");
        setGrade(iGradedefault);
        etTemp.setText("");
        etDate.setText("");
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = dateFormat.format(date);
        etDate.setText(strDate);
        Log.d("NewBathingSiteFragment","date:"+strDate);

//        etName.setText("Koltur");
//        etDesc.setText("Koltur desc");
//        etAddress.setText("Koltur");
//        etLatitude.setText("62");
//        etLongitude.setText("-7");
//        setGrade(2);
//        etTemp.setText("13");

    }

    public String getName() {
        return etName.getText().toString();
    }

    public String getDesc() {
        return etDesc.getText().toString();
    }

    public String getAddress() {
        return etAddress.getText().toString();
    }

    public Double getLatitude() {
        Double res = null;
        Log.d("NewBathingSiteFragment","getLatt begin");
        try {
            res=Double.parseDouble(etLatitude.getText().toString().trim());
            Log.d("NewBathingSiteFragment","afterConvert");
        }
        catch (NumberFormatException e) {
            res = null;
            Log.d("NewBathingSiteFragment","exception");
        };
        Log.d("NewBathingSiteFragment","end");
        return res;
    }

    public Double getLongitude() {
        Double res = null;
        Log.d("NewBathingSiteFragment","getLong begin");
        try {
            res=Double.parseDouble(etLongitude.getText().toString().trim());
            Log.d("NewBathingSiteFragment","afterConvert");
        }
        catch (NumberFormatException e) {
            res = null;
            Log.d("NewBathingSiteFragment","exception");
        };
        Log.d("NewBathingSiteFragment","end");
        return res;
    }

    public Float getGrade() {
        Float res = null;
        try {
            if (grade>=0 && grade<=5)
                res=(grade*1.0f);
            else
                throw new NumberRangeException();
        }
        catch (NumberFormatException | NumberRangeException e) {
            res = null;
        };
        return res;
    }

    public Float getTemp() {
        Float res = null;
        try {
            res=Float.parseFloat(etTemp.getText().toString().trim());
        }
        catch (NumberFormatException e) {
            res = null;
        };
        return res;
    }

    public void setLatitudeText(String val) {
        etLatitude.setText(val);
    }

    public void setLongitudeText(String val) {
        etLongitude.setText(val);
    }


    public Date getDate() {
        String sDate =  etDate.getText().toString().trim();
        Log.d("NewBathingSiteFragment","before convert dateString"+sDate);
        Date res = null;
        if (res==null) {
            try {
                Log.d("NewBathingSiteFragment","before convert dd/MM/yyyy");
                res = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
                Log.d("NewBathingSiteFragment","after convert dd/MM/yyyy");
            } catch (NumberFormatException | ParseException e) {
                res = null;
            }
        }
        if (res==null) {
            try {
                Log.d("NewBathingSiteFragment","before convert dd-MM-yyyy");
                res = new SimpleDateFormat("dd-MM-yyyy").parse(sDate);
                Log.d("NewBathingSiteFragment","after convert dd-MM-yyyy");
            } catch (NumberFormatException | ParseException e) {
                res = null;
            }
        }
        if (res==null) {
            try {
                res = new SimpleDateFormat("dd-MMM-yyyy").parse(sDate);
            } catch (NumberFormatException | ParseException e) {
                res = null;
            }
        }
        if (res==null) {
            try {
                Log.d("NewBathingSiteFragment","before date");
                res = Calendar.getInstance().getTime();
                Log.d("NewBathingSiteFragment","after date+"+res.toString());
            } catch (NumberFormatException e) {
                res = null;
            }
        }

        return res;
    }

    public void setGrade(int grade) {
        this.grade = grade;
        Drawable d0 = getResources().getDrawable(R.drawable.ic_star_border_orange_24dp);
        Drawable d1 = getResources().getDrawable(R.drawable.ic_star_orange_24dp);
        switch (this.grade) {
            case (1) :
                ivStar1.setImageDrawable(d1);
                ivStar2.setImageDrawable(d0);
                ivStar3.setImageDrawable(d0);
                ivStar4.setImageDrawable(d0);
                ivStar5.setImageDrawable(d0);
                break;
            case (2) :
                ivStar1.setImageDrawable(d1);
                ivStar2.setImageDrawable(d1);
                ivStar3.setImageDrawable(d0);
                ivStar4.setImageDrawable(d0);
                ivStar5.setImageDrawable(d0);
                break;
            case (3) :
                ivStar1.setImageDrawable(d1);
                ivStar2.setImageDrawable(d1);
                ivStar3.setImageDrawable(d1);
                ivStar4.setImageDrawable(d0);
                ivStar5.setImageDrawable(d0);
                break;
            case (4) :
                ivStar1.setImageDrawable(d1);
                ivStar2.setImageDrawable(d1);
                ivStar3.setImageDrawable(d1);
                ivStar4.setImageDrawable(d1);
                ivStar5.setImageDrawable(d0);
                break;
            case (5) :
                ivStar1.setImageDrawable(d1);
                ivStar2.setImageDrawable(d1);
                ivStar3.setImageDrawable(d1);
                ivStar4.setImageDrawable(d1);
                ivStar5.setImageDrawable(d1);
                break;
            default:
                this.grade=0;
                ivStar1.setImageDrawable(d0);
                ivStar2.setImageDrawable(d0);
                ivStar3.setImageDrawable(d0);
                ivStar4.setImageDrawable(d0);
                ivStar5.setImageDrawable(d0);
        }
    }


}
