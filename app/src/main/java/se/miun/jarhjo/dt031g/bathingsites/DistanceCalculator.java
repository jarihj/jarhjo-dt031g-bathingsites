package se.miun.jarhjo.dt031g.bathingsites;

// FROM: https://www.geeksforgeeks.org/program-distance-two-points-earth/
// FROM https://www.movable-type.co.uk/scripts/latlong.html
// Java program to calculate Distance Between
// Two Points on Earth
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.*;
import java.lang.*;

class DistanceCalculator {
    private static final double EARTH_RADIUS = 6371008.8d;

    public static LatLng intermediatePoint(LatLng p1, LatLng p2, double f) {
        return intermediatePoint(p1.latitude,p1.longitude,p2.latitude,p2.longitude,f);
    }

    /**
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @param f
     * FROM https://www.movable-type.co.uk/scripts/latlong.html
     */
    public static LatLng intermediatePoint(double lat1, double lon1, double lat2, double lon2, double f) {
        // Log.d("Distance",String.format("koord: %f   %f      %f    %f",lat1,lon1,lat2,lon2));
        double d = distanceSpheriod(lat1,lon1,lat2,lon2);
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Log.d("Distance","d:"+d);
        double r = EARTH_RADIUS;
        // Log.d("Distance","r:"+r);
        double delta = d/r;
        // Log.d("Distance","delta:"+delta);
        // lat= φ
        // lon:  λ
        // angular distance
        //     a = sin((1−f)⋅δ) / sin δ
        double a = Math.sin((1-f)*delta) / Math.sin(delta);
        //     b = sin(f⋅δ) / sin δ
        double b = Math.sin(f*delta) / Math.sin(delta);
        //     x = a ⋅ cos φ1 ⋅ cos λ1 + b ⋅ cos φ2 ⋅ cos λ2
        double x = a*Math.cos(lat1)*Math.cos(lon1)+b*Math.cos(lat2)*Math.cos(lon2);
        //     y = a ⋅ cos φ1 ⋅ sin λ1 + b ⋅ cos φ2 ⋅ sin λ2
        double y = a*Math.cos(lat1)*Math.sin(lon1)+b*Math.cos(lat2)*Math.sin(lon2);
        //     z = a ⋅ sin φ1 + b ⋅ sin φ2
        double z = a*Math.sin(lat1)+b*Math.sin(lat2);
        //     φi = atan2(z, √x² + y²)
        double lat_i = Math.atan2(z,Math.sqrt(sqr(x)+sqr(y)));
        // Log.d("Distance","lat_i:"+lat_i);
        //λi = atan2(y, x)
        double lon_i = Math.atan2(y,x);
        //where 	f is fraction along great circle route (f=0 is point 1, f=1 is point 2), δ is the angular distance d/R between the two points.
        lat_i=toDeg(lat_i);
        lon_i=toDeg(lon_i);
        // Log.d("Distance","lat_i:"+lat_i);
        // Log.d("Distance","lon_i:"+lon_i);
        return new LatLng(lat_i,lon_i);
    }

    public static double sqr(double x){
        return x*x;
    }
    /**
     * Calculate distance between to point on earth, modelled by a sphere.
     * This code is contributed by Prasad Kshirsagar
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return distance in meters
     * FROM https://www.movable-type.co.uk/scripts/latlong.html
     */
    public static double distanceSpheriod(double lat1, double lon1, double lat2, double lon2)
    {
        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

//        double c = 2 * Math.asin(Math.sqrt(a));
        double c = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a));

        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = EARTH_RADIUS; // meters

        // calculate the result
        return(c * r);
    }

    private static double toRad(double d) { return d/180.0d*Math.PI;}
    private static double toDeg(double r) { return r*180.0d/Math.PI;}

    /**
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return @return distance in meters
     * From https://fypandroid.wordpress.com/2011/08/10/vincenty-formula-for-distance-between-two-latitudelongitude-points/
     */
    public static double distanceElipsoid(double lat1, double lon1, double lat2, double lon2)
    {
        double cosSqAlpha;
        double sinSigma;
        double cos2SigmaM;
        double cosSigma;
        double sigma;
        double a = 6378137, b = 6356752.314245,  f = 1/298.257223563;  // WGS-84 ellipsoid params
        double L = toRad(lon2-lon1);
        double U1 = Math.atan((1-f) * Math.tan(toRad(lat1)));
        double U2 = Math.atan((1-f) * Math.tan(toRad(lat2)));
        double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
        double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);

        double lambda = L, lambdaP, iterLimit = 100;
        do {
            double sinLambda = Math.sin(lambda);
            double cosLambda = Math.cos(lambda);
            sinSigma = Math.sqrt((cosU2*sinLambda) * (cosU2*sinLambda) +
                    (cosU1*sinU2-sinU1*cosU2*cosLambda) * (cosU1*sinU2-sinU1*cosU2*cosLambda));
            if (sinSigma==0) return 0;  // co-incident points
            cosSigma = sinU1*sinU2 + cosU1*cosU2*cosLambda;
            sigma = Math.atan2(sinSigma, cosSigma);
            double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
            cosSqAlpha = 1 - sinAlpha*sinAlpha;
            cos2SigmaM = cosSigma - 2*sinU1*sinU2/cosSqAlpha;
            if (Double.isNaN(cos2SigmaM)) cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (§6)
            double C = f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha));
            lambdaP = lambda;
            lambda = L + (1-C) * f * sinAlpha *
                    (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)));
        } while (Math.abs(lambda-lambdaP) > 1e-12 && --iterLimit>0);

        if (iterLimit==0) return Double.NaN;  // formula failed to converge

        double uSq = cosSqAlpha * (a*a - b*b) / (b*b);
        double A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
        double B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));
        double deltaSigma = B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)-
                B/6*cos2SigmaM*(-3+4*sinSigma*sinSigma)*(-3+4*cos2SigmaM*cos2SigmaM)));
        double s = b*A*(sigma-deltaSigma);

//        s = s.toFixed(3); // round to 1mm precision
        return s;

//        // note: to return initial/final bearings in addition to distance, use something like:
//        var fwdAz = Math.atan2(cosU2*sinLambda,  cosU1*sinU2-sinU1*cosU2*cosLambda);
//        var revAz = Math.atan2(cosU1*sinLambda, -sinU1*cosU2+cosU1*sinU2*cosLambda);
//        return { distance: s, initialBearing: fwdAz.toDeg(), finalBearing: revAz.toDeg() };
    }
}



