package se.miun.jarhjo.dt031g.bathingsites;

import android.util.Log;

import java.util.Calendar;

public class BathingSiteParser {
    public static BathingSite parse(String s) {
        String[] sa;
        String[] saPos;
        String[] saDesc;
        String sName="", sDesc="", sAddress=null, sLat = "", sLon = "";
        for (int i = 0; i < s.length(); i++) {
            if (((int) s.charAt(i))>255) {
                Log.d("DownloadTask",String.format("Strange char %d found at pos %d",(int) s.charAt(i),i));
                s=s.replace(s.charAt(i),'\t');
            }
        } s=s.replaceAll("\t","");

        sa=s.split("\"\"");
//        Log.d("DownloadTask", "sa:" +sa.length);
//        for (int i=0; i<sa.length;i++) {
//            Log.d("DownloadTask", "sa "+i+":"+sa[i]);
//        }
//        Log.d("DownloadTask", "sa[0] : "+sa[0]);
        sa[0]=sa[0].replaceFirst("\""," ");
//        Log.d("DownloadTask", "sa[0] : "+sa[0]);
        sa[0]=sa[0].trim();
//        Log.d("DownloadTask", "sa[0] : "+sa[0]);
//        Log.d("DownloadTask", "sa[0] : "+sa[0].length());
        saPos=sa[0].split(",");
        sLat = saPos[1].trim();
        sLon = saPos[0].trim();
//        Log.d("DownloadTask", "Lat :"+sLat);
//        Log.d("DownloadTask", "Lat:"+sLat);
//        sLat=sLat.substring(0);
//        Log.d("DownloadTask", "Lat:"+sLat);
//        Log.d("DownloadTask", "Lat:"+sLat.length());
//        Log.d("DownloadTask", "Lat:"+sLat.indexOf("1"));
//        Log.d("DownloadTask", "Lon:"+sLon);
//        Log.d("DownloadTask", "Lon:"+sLon.length());
        if (sLat.charAt(0)>100) {
            Log.d("DownloadTask", String.format("sLat Ord:: %d  %d  %d  %d",(int) sLat.charAt(0),(int) sLat.charAt(1),(int) sLat.charAt(2),(int) sLat.charAt(3)));
        }

        saDesc=sa[1].trim().split("\"\"");
//        Log.d("DownloadTask", "Desc: "+saDesc[0]);
        int i = saDesc[0].indexOf(",");
        if (i!=-1) {
            sName = saDesc[0].substring(0, i).trim();
//            Log.d("DownloadTask", "Name: " + sName);
            sAddress = saDesc[0].substring(i + 1).trim();
//            Log.d("DownloadTask", "Addr: " + sAddress);
        } else {
            sName=saDesc[0].trim();
//            Log.d("DownloadTask", "Name: " + sName);
        }
        Double a,c;
        a = Double.valueOf(sLat.trim());
        c = Double.valueOf(sLon.trim());

        return new BathingSite(sName,null,sAddress,a,c,null,null, Calendar.getInstance().getTime());
    }
}
