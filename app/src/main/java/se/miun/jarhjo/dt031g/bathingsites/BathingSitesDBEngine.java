package se.miun.jarhjo.dt031g.bathingsites;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import static se.miun.jarhjo.dt031g.bathingsites.BSDBEOperation.*;

enum BSDBEOperation  {
    add, count, deleteAll, getAll
}

class BathingSiteEntry {
    public BSDBEOperation operation = null;
    public BathingSite entry = null;
    public BathingSiteEntry(BSDBEOperation operation, BathingSite entry) {
        this.operation = operation;
        this.entry = entry;
    }

}

public class BathingSitesDBEngine extends AsyncTask<BathingSiteEntry, Void, Void> {
    public static final double LOffGlobe = 200d;

    private Context context = null;
    private BathingSiteReader bathingSiteReader = null;
    private BathingSiteEntry bathingSiteEntry = null;
    private BSDBEOperation operation = null;
    private List<BathingSite> bathingSites = null;
    private int bathingSiteCount = -1;

    public static long getIDmillis() {
        return System.currentTimeMillis();
    }


    BathingSitesDBEngine(Context context) {
        this.context =  context;
    }

    BathingSitesDBEngine(Context context,BathingSiteReader bathingSiteReader) {
        this.context =  context;
        this.bathingSiteReader = bathingSiteReader;
    }

    @Override
    protected Void doInBackground(BathingSiteEntry... entries) {
        bathingSiteEntry = entries[0];
        operation = entries[0].operation;
        BathingSite bs = entries[0].entry;
        BathingSitesDatabase database;
        switch (operation) {
            case add:
                Log.d("BathingSitesDatabase", ": [" + operation + "]");
                long id             = bs.id;
                String name         = bs.name;
                String desc         = bs.desc;
                String address      = bs.address;
                if (bs.latitude==null)
                    bs.latitude=LOffGlobe+bs.id;
                Double latitude      = bs.latitude;
                if (bs.longitude==null)
                    bs.longitude=LOffGlobe+bs.id;
                Double longitude     = bs.longitude;
                Float grade         = bs.grade;
                Float temp          = bs.temp;
                Long date           = bs.date;
                database = BathingSitesDatabase.getInstance(context);
                database.addBathingSite(bs);
                break;
            case count:
                Log.d("BathingSitesDatabase", ": [" + operation + "]");
                database = BathingSitesDatabase.getInstance(context);
                bathingSiteCount = database.countBathingSites();
                break;
            case deleteAll:
                Log.d("BathingSitesDatabase", ": [" + operation + "]");
                database = BathingSitesDatabase.getInstance(context);
                database.deleteAll();
                break;
            case getAll:
                Log.d("BathingSitesDatabase", ": [" + operation + "]");
                database = BathingSitesDatabase.getInstance(context);
                bathingSites=database.getAll();
                break;
            default:
                Log.d("BathingSitesDatabase", "Default action: [" + operation + "]");
        }

        return null;
    }


    protected void onPostExecute(Void values) {
        if (bathingSiteReader!=null)
            switch (operation) {
                case add:
                    Log.d("BathingSitesDatabase", "onPostExecute: [" + operation + "]");
                    bathingSiteReader.onBathingSiteAdd(bathingSiteEntry.entry);
                    break;
                case count:
                    Log.d("BathingSitesDatabase", "onPostExecute: [" + operation + "]");
                    bathingSiteReader.onBathingSiteCount(bathingSiteCount);
                    break;
                case deleteAll:
                    Log.d("BathingSitesDatabase", "onPostExecute: [" + operation + "]");
                    bathingSiteReader.onBathingSiteDelete();
                    break;
                case getAll:
                    Log.d("BathingSitesDatabase", "onPostExecute: [" + operation + "]");
                    bathingSiteReader.onBathingSiteRead(bathingSites);
                    break;
                default:
                    Log.d("BathingSitesDatabase", "onPostExecute: Default action: [" + operation + "]");
            }

        this.context = null;
    }
}