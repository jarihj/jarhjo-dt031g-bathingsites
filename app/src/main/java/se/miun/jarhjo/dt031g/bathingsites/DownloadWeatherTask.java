package se.miun.jarhjo.dt031g.bathingsites;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

class WeatherData {
    Integer id = null;
    String name = null;
    Double latitude = null;
    Double longitude = null;
    String icon = null;
    String description = null;
    Double temperature = null;

    public static WeatherData buildParseJSON(String weatherData) {
        WeatherData res = null, wd = new WeatherData();
        String strTitle = "NO TITLE";
        String strMessage = "NO MESSAGE";
        String name = "", lon = "", lat = "", main = "", description = "", icon = "", temp = "";
        int id = -1;
        try {
            JSONObject rootObject = new JSONObject(weatherData);
            JSONObject coordObject = rootObject.getJSONObject("coord");
            lon = coordObject.getString("lon");
            wd.longitude = Double.parseDouble(lon);
            lat = coordObject.getString("lat");
            wd.latitude = Double.parseDouble(lat);
            Log.d("NewBathingSiteActivity", "readWeatherData: lon: " + lon);
            Log.d("NewBathingSiteActivity", "readWeatherData: lat: " + lat);
            JSONArray weatherArray = rootObject.getJSONArray("weather");
            Log.d("NewBathingSiteActivity", "readWeatherData: weatherArray: " + weatherArray);
            JSONObject weatherObject = (JSONObject) weatherArray.get(0);
            Log.d("NewBathingSiteActivity", "readWeatherData: weatherObject: " + weatherObject);
            id = weatherObject.getInt("id");
            wd.id = id;
            main = weatherObject.getString("main");
            description = weatherObject.getString("description");
            wd.description = description;
            icon = weatherObject.getString("icon");
            wd.icon = icon;
            Log.d("NewBathingSiteActivity", "readWeatherData: icon: " + icon);
            JSONObject mainObject = rootObject.getJSONObject("main");
            temp = mainObject.getString("temp");
            wd.temperature = Double.parseDouble(temp);
            Log.d("NewBathingSiteActivity", "readWeatherData: temp: " + temp);
            name = rootObject.getString("name");
            wd.name = name;
            Log.d("NewBathingSiteActivity", "readWeatherData: name: " + name);

            strMessage = String.format("name:%s\nlat:%s\nlon:%s\nicon:%s", name, lat, lon, icon);
            Log.d("NewBathingSiteActivity", "readWeatherData:: " + strMessage);
            res = wd;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }
}
//                                 AsyncTask<Params, Progress, Result>
public class DownloadWeatherTask extends AsyncTask<String, Integer, WeatherData> {
    String saveFile; // full path
    String fileName; // only filename
    String url = "";
    String dots = "";
    private ProgressDialog dialog;
    private Context context = null;
    private WeatherReader weatherReader;
    private Drawable drawable = null;

    public void setContext(Context context) {
        this.context=context;
    }

    public DownloadWeatherTask(Context context, WeatherReader weatherReader) {
        //
        this.context=context;
        this.weatherReader=weatherReader;
        dialog = new ProgressDialog(context); // nothing sensible
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("DownloadWeatherTask", "onPreExecute: starting");
        dialog.setMessage(context.getResources().getString(R.string.weather_current_getting_status));
        dialog.show();
    }

    protected WeatherData doInBackground(String... urls) {
        url = urls[0];
        int iCharsRead = 0, iTotalCharsRead = 0;
        int iSize = 0;
        WeatherData wd = null;

        try {
            Log.d("DownloadWeatherTask","doInBackground: url: "+url);
            URL u = new URL(url);
            URLConnection urlConnection = null;
            urlConnection = u.openConnection();

            InputStream inputStream = urlConnection.getInputStream();

            char[] buffer = new char[1024];
            iSize = urlConnection.getContentLength();
            Log.d("DownloadWeatherTask","before setprogress");
            Integer [] progressStruct = {0,iSize};
            StringBuilder stringBuilder = new StringBuilder();
            Reader in = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            while ((iCharsRead = in.read(buffer)) > 0) {
                stringBuilder.append(buffer,0,iCharsRead);
                iTotalCharsRead+=iCharsRead;
                progressStruct[0] = (int) iTotalCharsRead;
                publishProgress(progressStruct);
                Thread.sleep(200);
            }
            inputStream.close();
            String strRes=stringBuilder.toString();
            wd = WeatherData.buildParseJSON(strRes);
            Log.d("DownloadWeatherTask","readWeatherData: "+String.format("Download finished. %d bytes read of %d.",iTotalCharsRead,iSize));

            //=====================================================================================================
            try {
                publishProgress(progressStruct);
                String icon = "000";
                if (wd!=null) {
                    icon = wd.icon;
                }
                Log.d("DownloadWeatherTask","readWeatherData: icon: "+icon);
                String iconURL = context.getResources().getString(R.string.url_weathericons_base_url) + icon + ".png";
                Log.d("DownloadWeatherTask","readWeatherData: iconURL: "+iconURL);
                publishProgress(progressStruct);
                drawable = Drawable.createFromStream((InputStream) new URL(iconURL).getContent(),"");
                Log.d("DownloadWeatherTask","readWeatherData: drawable: "+drawable);
                publishProgress(progressStruct);
            }
            catch (MalformedURLException e) {Log.d("DownloadWeatherTask","readWeatherData: MalformedURLException: "+e);}
            catch (IOException e) {Log.d("DownloadWeatherTask","readWeatherData: IOException: "+e);}

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return wd;
    }

    private void iterateDots() {
        dots += ".";
        if (dots.length() > 3)
            dots = "";
    }

    protected void onProgressUpdate(Integer... progress) {
        Log.d("DownloadWeatherTask",String.format("Progress [%s] %d of %d.",url,progress[0],progress[1]));
        String status = context.getResources().getString(R.string.weather_current_getting_status);
        iterateDots();
        dialog.setMessage(String.format("%s %s", status, dots));
    }

    protected void onPostExecute(WeatherData result) {
        Log.d("DownloadWeatherTask","onPostExecute: ");

        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        if (weatherReader!=null) {
            weatherReader.onReadWeatherData(result,drawable);
        }
    }

}
