package se.miun.jarhjo.dt031g.bathingsites;

import android.graphics.drawable.Drawable;

import org.json.JSONException;

public interface WeatherReader {
    public void onReadWeatherData(WeatherData weatherData);
    public void onReadWeatherData(WeatherData weatherData, Drawable drawable);
}
