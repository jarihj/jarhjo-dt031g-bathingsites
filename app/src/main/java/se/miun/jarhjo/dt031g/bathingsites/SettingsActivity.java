package se.miun.jarhjo.dt031g.bathingsites;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class SettingsActivity extends AppCompatActivity {


    private static boolean isEmptyValue(String val) {
        return ( (val==null) | (val.equals("")) | (val.equals("Default value")) | (val.equals("novalue"))   );
    }
    private static String getCustomSetting(Context context, String key, String defaultValue) {
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        String res = sh.getString(key, defaultValue);
        if (isEmptyValue(res))
            res = defaultValue;
        return res;
    }

    private static Double getCustomSettingDouble(Context context, String key, String defaultValue) {
        Double res = null;
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            res = Double.valueOf(sh.getString(key, defaultValue));
            if (res==null)
                res = Double.valueOf(defaultValue);
        } catch (NumberFormatException e) {
            res=null;
        }
        return res;

    }


    public static String getWeatherServerURL(Context context) {
        return getCustomSetting(context,context.getResources().getString(R.string.key_weather_server_url),context.getResources().getString(R.string.url_weather_base_url));
    }

    public static String getBathingSiteDownloadURL(Context context) {
        return getCustomSetting(context,context.getResources().getString(R.string.key_bathing_sites_url),context.getResources().getString(R.string.url_bathing_sites_url));
    }

    public static double getMapRadius(Context context) {
        return getCustomSettingDouble(context,context.getResources().getString(R.string.key_map_radius_km),context.getResources().getString(R.string.double_map_radius_km));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.settings_activity_root_preferences, rootKey);
        }
    }
}